class Loaders{
	
	OBJLoadModel(urlTexture, urlObj, callback){

		var self = this;
		function objCallback(model){
			model.traverse(function (child) {
				if(child instanceof THREE.Mesh){
					child.material.map = self.texture;
					child.castShadow = true;
				}
			});
			self.model = model;
			callback(model);	
		}	
	
		function texCallBack(texture){
			var objloader = new THREE.OBJLoader();
			self.texture = texture;
			objloader.load(urlObj, objCallback);
		}
			
		this.callback = callback;
	
		var textureloader = new THREE.TextureLoader();
		textureloader.load(urlTexture, texCallBack, undefined, function(err) { console.error('Error: cannot load model');} );
	} // OBJloadModel
	
	
	DaeLoadModel(urlObj, callback){
	
		var self = this;
		function daeCallback(collada){
			self.collada = collada;
			callback(collada);
		}	
			
		this.callback = callback;
	
		var daeloader = new THREE.ColladaLoader();
		daeloader.load(urlObj, daeCallback);

	} // DaeLoadModel


	MTLLoadModel(urlMTL, urlObj, callback){
		
		var self = this;
		function objCallback(mesh){
			mesh.traverse(function (child) {
				if(child instanceof THREE.Mesh){
					child.castShadow = true;
				}
			});
			self.mesh = mesh;
			callback(mesh);
		}
		
		function mtlCallback(materials){
			materials.preload();
			var objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.load(urlObj, objCallback);
		}
		var mtlLoader = new THREE.MTLLoader();
		mtlLoader.load(urlMTL, mtlCallback);
	}


}

// Slenderman Model: https://free3d.com/3d-model/slenderman-from-slender-the-arrival-79467.html
// Key Model: https://www.turbosquid.com/3d-models/old-key-3ds/448201


/*
	var mtlLoader = new THREE.MTLLoader();
		mtlLoader.load("models/key/key.mtl", function(materials){
			materials.preload();
			var objLoader = new THREE.OBJLoader();
			objLoader.setMaterials(materials);
			objLoader.load("models/key/key.obj", function(mesh){
				mesh.scale.set(2.5,2.5,2.5);
				mesh.position.set(-3,-3,-3);
				scene.add(mesh);
			});
		});
*/

/*
var loader = new THREE.ColladaLoader();
loader.load( 'models/boy/walking/walking.dae', function ( collada ) {

	var animations = collada.animations;
	var avatar = collada.scene;
	
	avatar.scale.set(1.5,1.5,1.5);
	scene.add( avatar );
	
	mixer = new THREE.AnimationMixer(avatar);
	animation = mixer.clipAction(animations[0]);
	animation.setLoop(THREE.LoopOnce);
	animation.clampWhenFinished = true; //pause on last frame
	animation.play();
	mixers.push(mixer);

});

	https://threejs.org/docs/#api/animation/AnimationAction.repetitions

*/



/*
	daeloader.load(urlObj, function (collada) {
			//cute_turtle_07.dae
            var child = collada.skins[0];
            scene.add(child);


            var animation = new THREE.Animation(child, child.geometry.animation);
            animation.play();
            child.position.y = -60;
        });

*/
